/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.citamedica.dao;

import cl.citamedica.entity.CitaMedica;
import cl.citamedica.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author catal
 */
public class CitaMedicaJpaController implements Serializable {

    public CitaMedicaJpaController() {
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU_CITA");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CitaMedica citaMedica) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(citaMedica);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CitaMedica citaMedica) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            citaMedica = em.merge(citaMedica);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = citaMedica.getId();
                if (findCitaMedica(id) == null) {
                    throw new NonexistentEntityException("The citaMedica with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CitaMedica citaMedica;
            try {
                citaMedica = em.getReference(CitaMedica.class, id);
                citaMedica.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The citaMedica with id " + id + " no longer exists.", enfe);
            }
            em.remove(citaMedica);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CitaMedica> findCitaMedicaEntities() {
        return findCitaMedicaEntities(true, -1, -1);
    }

    public List<CitaMedica> findCitaMedicaEntities(int maxResults, int firstResult) {
        return findCitaMedicaEntities(false, maxResults, firstResult);
    }

    private List<CitaMedica> findCitaMedicaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CitaMedica.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CitaMedica findCitaMedica(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CitaMedica.class, id);
        } finally {
            em.close();
        }
    }

    public int getCitaMedicaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CitaMedica> rt = cq.from(CitaMedica.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

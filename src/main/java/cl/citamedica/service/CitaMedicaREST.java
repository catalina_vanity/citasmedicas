/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.citamedica.service;

import cl.citamedica.entity.CitaMedica;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import cl.citamedica.dao.CitaMedicaJpaController;
import cl.citamedica.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import javax.ws.rs.core.Response;

/**
 *
 * @author catal
 */
@Stateless
@Path("citamedica")
public class CitaMedicaREST {
    
    private CitaMedicaJpaController citaDao= new CitaMedicaJpaController();
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(CitaMedica entity) {
        
        if(this.citaDao.findCitaMedica(entity.getId())!=null)
        {
            return Response.status(400).entity("{\"Error\":\"Cita ya existe\"}").build();
        }
        this.citaDao.create(entity);
        return Response.ok(200).build();
        
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response edit(@PathParam("id") Long id, CitaMedica entity) throws Exception {
        if(this.citaDao.findCitaMedica(id)!=null)
        {
            this.citaDao.edit(entity);
            return Response.ok(200).entity(entity).build();
        }
        return Response.status(400).entity("{\"Error\":\"Cita no existe\"}").build();
    }

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") Long id) throws NonexistentEntityException {
        
         if(this.citaDao.findCitaMedica(id)!=null)
        {
            this.citaDao.destroy(id);
            return Response.ok(200).build();
        }
        return Response.status(400).entity("{\"Error\":\"Cita no existe\"}").build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(@PathParam("id") Long id) {
        CitaMedica cita=this.citaDao.findCitaMedica(id);
        if(cita!=null)
        {
            return Response.ok(200).entity(cita).build();
        }
        return Response.status(400).entity("{\"Error\":\"Cita no existe\"}").build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        List<CitaMedica> citas=citaDao.findCitaMedicaEntities();
        return Response.ok(200).entity(citas).build();
    }

}

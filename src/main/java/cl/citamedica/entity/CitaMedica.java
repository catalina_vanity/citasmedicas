/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.citamedica.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author catal
 */
@Entity
@Table(name = "cita_medica")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CitaMedica.findAll", query = "SELECT c FROM CitaMedica c"),
    @NamedQuery(name = "CitaMedica.findByNombrePaciente", query = "SELECT c FROM CitaMedica c WHERE c.nombrePaciente = :nombrePaciente"),
    @NamedQuery(name = "CitaMedica.findByNombreMedico", query = "SELECT c FROM CitaMedica c WHERE c.nombreMedico = :nombreMedico"),
    @NamedQuery(name = "CitaMedica.findById", query = "SELECT c FROM CitaMedica c WHERE c.id = :id"),
    @NamedQuery(name = "CitaMedica.findByFechaCita", query = "SELECT c FROM CitaMedica c WHERE c.fechaCita = :fechaCita"),
    @NamedQuery(name = "CitaMedica.findByEspecialidad", query = "SELECT c FROM CitaMedica c WHERE c.especialidad = :especialidad"),
    @NamedQuery(name = "CitaMedica.findByEstado", query = "SELECT c FROM CitaMedica c WHERE c.estado = :estado")})
public class CitaMedica implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre_paciente")
    private String nombrePaciente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre_medico")
    private String nombreMedico;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_cita")
    @Temporal(TemporalType.DATE)
    private Date fechaCita;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "especialidad")
    private String especialidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "estado")
    private String estado;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    public CitaMedica() {
    }

    public CitaMedica(Long id) {
        this.id = id;
    }

    public CitaMedica(Long id, String nombrePaciente, String nombreMedico, Date fechaCita, String especialidad, String estado) {
        this.id = id;
        this.nombrePaciente = nombrePaciente;
        this.nombreMedico = nombreMedico;
        this.fechaCita = fechaCita;
        this.especialidad = especialidad;
        this.estado = estado;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    public String getNombreMedico() {
        return nombreMedico;
    }

    public void setNombreMedico(String nombreMedico) {
        this.nombreMedico = nombreMedico;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(Date fechaCita) {
        this.fechaCita = fechaCita;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CitaMedica)) {
            return false;
        }
        CitaMedica other = (CitaMedica) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.controlapi.entity.CitaMedica[ id=" + id + " ]";
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}

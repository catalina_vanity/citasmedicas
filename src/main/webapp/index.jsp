<%-- 
    Document   : index
    Created on : 03-07-2021
    Author     : Catalina Gonzalez
    seccion    : 6
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluación Nº3 Catalina Gonzalez</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Evaluacion 3 Citas Medicas</a>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse1">Crear cita</a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-heading">POST https://citasmedicas6.herokuapp.com/api/citamedica/
                            <br>
                            Request body:
                        </div>
                        <div class="panel-body"> 
                            <code>
                                {
                                "especialidad": "Especialidad de prueba",
                                "estado": "Activa",
                                "fechaCita": "2021-07-08T12:00:00",
                                "id": 1,
                                "nombreMedico": "test",
                                "nombrePaciente": "test"
                                }
                            </code>
                        </div>
                        <div class="panel-footer">Crea una cita con el body de ejemplo</div>
                    </div>
                </div>
            </div>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse2">Editar cita</a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-heading">PUT https://citasmedicas6.herokuapp.com/api/citamedica/
                            <br>
                            Request body:
                        </div>
                        <div class="panel-body"> 
                            <code>
                                {
                                "especialidad": "Especialidad de prueba",
                                "estado": "Activa",
                                "fechaCita": "2021-07-08T12:00:00",
                                "id": 1,
                                "nombreMedico": "test",
                                "nombrePaciente": "test"
                                }
                            </code>
                        </div>
                        <div class="panel-footer">Edita una cita con el body de ejemplo</div>
                    </div>
                </div>
            </div>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse3">Listar citas</a>
                        </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-heading">GET https://citasmedicas6.herokuapp.com/api/citamedica/
                            <br>
                            Response:  
                        </div>
                        <div class="panel-body"> 
                            <code>
                               [
                                {
                                "especialidad": "Especialidad de prueba",
                                "estado": "Activa",
                                "fechaCita": "2021-07-03T04:00:00Z[UTC]",
                                "id": 1,
                                "nombreMedico": "test",
                                "nombrePaciente": "test"
                                },
                                {
                                "especialidad": "Nutricionista",
                                "estado": "Activa",
                                "fechaCita": "2021-07-08T12:00:00Z[UTC]",
                                "id": 2,
                                "nombreMedico": "test",
                                "nombrePaciente": "test"
                                }
                               ]
                            </code>
                        </div>
                        <div class="panel-footer">Obtiene la lista de las citas creadas</div>
                    </div>
                </div>
            </div>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse4">Obtener cita por Id</a>
                        </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse">
                          <div class="panel-heading">GET https://citasmedicas6.herokuapp.com/api/citamedica/{id}
                            <br>
                            Response:  
                        </div>
                        <div class="panel-body"> 
                            <code>
                               
                                {
                                "especialidad": "Especialidad de prueba",
                                "estado": "Activa",
                                "fechaCita": "2021-07-03T04:00:00Z[UTC]",
                                "id": 1,
                                "nombreMedico": "test",
                                "nombrePaciente": "test"
                                }
                               
                            </code>
                        </div>
                        <div class="panel-footer">Obtiene una cita por su Id</div>
                    </div>
                </div>
            </div>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse5">Elimina una cita por Id</a>
                        </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-heading">DELETE https://citasmedicas6.herokuapp.com/api/citamedica/{id} </div>
                        <div class="panel-footer">Eliminar una cita por su Id o Bad Request si no existe la cita</div>
                    </div>
                </div>
            </div>
        </div>
        
        
    </body>
</html>
